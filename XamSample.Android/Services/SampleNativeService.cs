﻿using XamSample.Services;

namespace XamSample.Droid.Services
{
    /// <summary>
    /// This service doesn't really do anything but show how to implement a service
    /// within each platform project. If you need access to an Android-specific API,
    /// this is the place to do it.
    /// </summary>
    public class SampleNativeService : ISampleNativeService
    {
        public string GetNativeMessage()
        {
            return "Hello from the Android Project!";
        }
    }
}
