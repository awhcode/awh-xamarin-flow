# AWH Xamarin Flow Sample

Sample Xamarin.Forms app app using the AWH Xamarin Flow setup.

This repo is to demonstrate how to build a Xamarin.Forms app using MVVM, HostBuilder, and ASP.NET Core-style dependency injection. This app does nothing and is only provided as a demonstration of these concepts.
