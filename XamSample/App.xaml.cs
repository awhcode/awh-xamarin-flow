﻿using System;
using Xamarin.Forms;
using XamSample.Services;
using XamSample.ViewModels;
using XamSample.Views;

namespace XamSample
{
    public partial class App : Application
    {
        /// <summary>
        /// This should only be used when normal dependency injection cannot be used such
        /// as when the service retrieved is dynamic or when your are outside of the normal
        /// scope of our dependency injection.
        /// </summary>
        public static IServiceProvider? ServiceProvider { get; set; } = null;

        private INavigatorService _navigatorService;
        private IPageService _pageService;

        public App(INavigatorService navigatorService, IPageService pageService)
        {
            InitializeComponent();

            _navigatorService = navigatorService;
            _pageService = pageService;

            MainPage = _navigatorService.GetMainPage<ItemListPage>();
        }

        /// <summary>
        /// This override method is needed because we have to set MainPage in the constructor
        /// but we cannot use the async initialize method on the ViewModel in the constructor.
        /// This method can be made async, but cannot return a task since it is an override,
        /// so it should be the only place you will have an "async void" method.
        /// </summary>
        protected override async void OnStart()
        {
            var rootPage = _navigatorService.GetRootPage();

            // This should be the only place in the app where the Page Service is used outside
            // of the Navigator Service and only because we have to initialize a page in a
            // separate method than when we loaded that page.
            await _pageService.InitializePageAsync(rootPage);
        }

        protected override void OnSleep() { }
        protected override void OnResume() { }
    }
}
