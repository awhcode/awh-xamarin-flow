﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamSample.Services
{
    public class NavigatorService : INavigatorService
    {
        private readonly IPageService _pageService;

        // This default value will be overwritten when GetMainPage is called.
        private NavigationPage _navigationPage = new NavigationPage();

        public NavigatorService(IPageService pageService)
        {
            _pageService = pageService;
        }

        /// <summary>
        /// Creates the main NavigationPage instance. This method should only
        /// be called once.
        /// </summary>
        /// <typeparam name="MainPageType"></typeparam>
        /// <returns></returns>
        public NavigationPage GetMainPage<MainPageType>()
        {
            var rootPage = _pageService.GetPage<MainPageType>();

            return _navigationPage = new NavigationPage(rootPage);
        }

        /// <summary>
        /// Gets the root page of the NavigationPage intance.
        /// </summary>
        /// <returns></returns>
        public Page GetRootPage() => _navigationPage.RootPage;

        /// <summary>
        /// Gets the current page of the navigation page instance (modals
        /// do not count as the current page)
        /// </summary>
        /// <returns></returns>
        public Page GetCurrentPage() => _navigationPage.CurrentPage;

        /// <summary>
        /// Pushes a new page to the navigation stack and initializes it.
        /// </summary>
        /// <typeparam name="NewPageType"></typeparam>
        /// <param name="initialData"></param>
        /// <returns></returns>
        public async Task PushAsync<NewPageType>(object? initialData = null)
        {
            var newPage = _pageService.GetPage<NewPageType>();

            await _pageService.InitializePageAsync(newPage, initialData);
            await _navigationPage.PushAsync(newPage);
        }

        /// <summary>
        /// Removes the top-most page from the navigation stack and
        /// refreshes the newly-visble page below it in the stack.
        /// </summary>
        /// <param name="updatedData"></param>
        /// <returns></returns>
        public async Task PopAsync()
        {
            await _navigationPage.PopAsync();
            await _pageService.RefreshPageAsync(GetCurrentPage());
        }

        /// <summary>
        /// Removes alll pages from the navigation stack and
        /// refreshes the root page in the stack.
        /// </summary>
        /// <param name="updatedData"></param>
        /// <returns></returns>
        public async Task PopToRootAsync()
        {
            await _navigationPage.PopToRootAsync();
            await _pageService.RefreshPageAsync(GetCurrentPage());
        }

        /// <summary>
        /// Opens a page as a modal.
        /// </summary>
        /// <typeparam name="NewModalPageType"></typeparam>
        /// <param name="initialData"></param>
        /// <returns></returns>
        public async Task PushModalAsync<NewModalPageType>(object? initialData = null)
        {
            var newPage = _pageService.GetPage<NewModalPageType>();

            await _pageService.InitializePageAsync(newPage, initialData);
            await GetCurrentPage().Navigation.PushModalAsync(newPage);
        }

        /// <summary>
        /// Closes a modal page. Refreshes the page that will be visible after closing.
        /// </summary>
        /// <param name="updatedData"></param>
        /// <returns></returns>
        public async Task PopModalAsync()
        {
            var currentPage = GetCurrentPage();

            await currentPage.Navigation.PopModalAsync();
            await _pageService.RefreshPageAsync(currentPage);
        }
    }
}
