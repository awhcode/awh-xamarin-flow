﻿namespace XamSample.Services
{
    /// <summary>
    /// Example of an interface to a service which has platform-specific implementations
    /// </summary>
    public interface ISampleNativeService
    {
        string GetNativeMessage();
    }
}
