﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamSample.Services
{
    public interface IPageService
    {
        Page GetPage<PageType>();
        Task InitializePageAsync(Page page, object? initialData = null);
        Task RefreshPageAsync(Page page);
    }
}