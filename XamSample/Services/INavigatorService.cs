﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamSample.Services
{
    public interface INavigatorService
    {
        NavigationPage GetMainPage<T>();

        Page GetRootPage();
        Page GetCurrentPage();

        Task PushAsync<T>(object? initialData = null);
        Task PopAsync();
        Task PopToRootAsync();

        Task PushModalAsync<T>(object? initialData = null);
        Task PopModalAsync();
    }
}