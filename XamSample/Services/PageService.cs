﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Xamarin.Forms;
using XamSample.ViewModels;
using XamSample.Views;

namespace XamSample.Services
{
    public class PageService : IPageService
    {
        /// <summary>
        /// Gets a page from the App ServiceProvider. Also gets the ViewModel for the
        /// page and sets the ViewModel as the page's BindingContext.
        /// </summary>
        /// <returns></returns>
        public Page GetPage<PageType>()
        {
            if (App.ServiceProvider == null)
                throw new InvalidOperationException("App.ServiceProvider is null and has not been set up correctly");

            var page = App.ServiceProvider.GetService<PageType>() as Page;

            if (page == null)
                throw new ArgumentException("Unable to locate page in ServiceProvider", nameof(PageType));

            if (page is IPageWithViewModel pageWithViewModel)
            {
                var viewModel = App.ServiceProvider.GetService(pageWithViewModel.ViewModelType) as BasePageViewModel;

                if (viewModel == null)
                    throw new ArgumentException("Unable to locate page view model in ServiceProvider", pageWithViewModel.ViewModelType.Name);

                page.BindingContext = viewModel;
            }

            return page;
        }

        /// <summary>
        /// Initializes the view model that is assigned as the page's binding context.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="initialData"></param>
        /// <returns></returns>
        public async Task InitializePageAsync(Page page, object? initialData = null)
        {
            if (page.BindingContext is BasePageViewModel baseViewModel)
                await baseViewModel.InitializeAsync(initialData);
        }

        /// <summary>
        /// Refreshes the view model that is assigned as the page's binding context.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="updatedData"></param>
        /// <returns></returns>
        public async Task RefreshPageAsync(Page page)
        {
            if (page.BindingContext is BasePageViewModel baseViewModel)
                await baseViewModel.RefreshAsync();
        }
    }
}
