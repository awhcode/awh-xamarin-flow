﻿using Microsoft.Extensions.Hosting;

namespace XamSample.Constants
{
    public static class Environment
    {
        // These options can be expanded to encompass more environments (ex Test, UAT, Demo, etc).
        // You will need to add more build configurations and set an new symbol to be used below
        // to set the Current value.
        public static readonly string Development = Environments.Development;
        public static readonly string Production = Environments.Production;

        // These symbols are set for each build configuration.
#if ENVIRONMENT_PROD
        public static readonly string Current = Production;
#else
        public static readonly string Current = Development;
#endif
    }
}
