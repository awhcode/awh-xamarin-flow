﻿using System;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Xamarin.Essentials;
using XamSample.Services;
using XamSample.ViewModels;
using XamSample.Views;

namespace XamSample
{
    public static class Startup
    {
        public static App Initialize(Action<HostBuilderContext, IServiceCollection> configureNativeServices)
        {
            // The GetManifestResourceStream functions cannot be executed from within the
            // ConfigureHostConfiguration's configureDelegate because that causes a "Stream
            // was not readable" ArgumentException.
            var assembly = Assembly.GetExecutingAssembly();
            var environment = Constants.Environment.Current;
            using var appSettingsStream = assembly.GetManifestResourceStream("XamSample.appsettings.json");
            using var appSettingsEnvironmentStream = assembly.GetManifestResourceStream($"XamSample.appsettings.{environment}.json");

            var host = new HostBuilder()
                .UseContentRoot(FileSystem.AppDataDirectory)
                .UseEnvironment(environment)
                .ConfigureHostConfiguration(config =>
                {
                    config.AddJsonStream(appSettingsStream);
                    config.AddJsonStream(appSettingsEnvironmentStream);
                })
                .ConfigureServices(configureNativeServices)
                .ConfigureServices(ConfigureServices)
                .ConfigureLogging(ConfigureLogging)
                .Build();

            App.ServiceProvider = host.Services;

            return App.ServiceProvider.GetService<App>() ?? throw new Exception("The App service provider isn't set up properly.");
        }

        private static void ConfigureServices(HostBuilderContext context, IServiceCollection services)
        {
            // Custom services for this app. Each service should have a matching
            // interface so it can be mocked for unit tests.
            services.AddSingleton<IPageService, PageService>();
            services.AddSingleton<INavigatorService, NavigatorService>();

            // Add Pages and their ViewModels as transients so they can be retrieved using DI
            services.AddTransient<ItemListPage>();
            services.AddTransient<ItemListViewModel>();
            services.AddTransient<ItemDetailPage>();
            services.AddTransient<ItemDetailViewModel>();

            // Finally register the App so it can access the NavigatorService
            services.AddSingleton<App>();
        }

        private static void ConfigureLogging(ILoggingBuilder logging)
        {
            logging.AddSimpleConsole(options =>
            {
                // Setup a console logger and disable colors since they don't have any colors in VS
                options.ColorBehavior = LoggerColorBehavior.Disabled;
            });
        }
    }
}
