﻿using System;
using Xamarin.Forms;
using XamSample.ViewModels;

namespace XamSample.Views
{
    public partial class ItemDetailPage : ContentPage, IPageWithViewModel
    {
        public Type ViewModelType => typeof(ItemDetailViewModel);

        public ItemDetailPage() => InitializeComponent();
    }
}
