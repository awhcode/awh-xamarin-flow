﻿using System;
using Xamarin.Forms;
using XamSample.ViewModels;

namespace XamSample.Views
{
    public partial class ItemListPage : ContentPage, IPageWithViewModel
    {
        public Type ViewModelType => typeof(ItemListViewModel);

        public ItemListPage() => InitializeComponent();
    }
}
