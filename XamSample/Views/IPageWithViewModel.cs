﻿using System;

namespace XamSample.Views
{
    public interface IPageWithViewModel
    {
        Type ViewModelType { get; }
    }
}
