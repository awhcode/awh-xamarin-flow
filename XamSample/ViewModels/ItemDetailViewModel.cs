﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using XamSample.Services;

namespace XamSample.ViewModels
{
    public class ItemDetailViewModel : BasePageViewModel
    {
        private IConfiguration _config;
        private ISampleNativeService _nativeService;

        private string _title = string.Empty;
        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        private string _configValue = string.Empty;
        public string ConfigValue
        {
            get => _configValue;
            set => SetProperty(ref _configValue, value);
        }

        private string _nativeMessage = string.Empty;
        public string NativeMessage
        {
            get => _nativeMessage;
            set => SetProperty(ref _nativeMessage, value);
        }

        public ItemDetailViewModel(IConfiguration config, ISampleNativeService nativeService, INavigatorService navigatorService)
            : base(navigatorService)
        {
            // Ideally, you will probably wrap your config in a service class so you
            // can use strongly typed values.
            _config = config;
            _nativeService = nativeService;
        }

        public override async Task InitializeAsync(object? initialData = null)
        {
            await base.InitializeAsync(initialData);

            if (initialData is string title)
                Title = title;

            ConfigValue = _config.GetValue<string>("SomeConfigValue");
            NativeMessage = _nativeService.GetNativeMessage();
        }
    }
}
