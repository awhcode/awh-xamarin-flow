﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Extensions.Logging;
using Xamarin.CommunityToolkit.ObjectModel;
using XamSample.Services;
using XamSample.Views;

namespace XamSample.ViewModels
{
    public class ItemListViewModel : BasePageViewModel
    {
        private ILogger<ItemListViewModel> _logger;

        private ObservableCollection<string> _items = new ObservableCollection<string>();
        public ObservableCollection<string> Items
        {
            get => _items;
            set => SetProperty(ref _items, value);
        }

        public ICommand OpenItemCommand => CommandFactory.Create<string>(OpenItemAsync);

        public ItemListViewModel(ILogger<ItemListViewModel> logger, INavigatorService navigatorService) : base(navigatorService)
        {
            _logger = logger;

            Items = new ObservableCollection<string>();
        }

        public override async Task InitializeAsync(object? initialData = null)
        {
            await base.InitializeAsync(initialData);

            _logger.LogInformation("List VM Initialize");

            // In a real app, this is where you would call a service which
            // interacts with an API or other data store.
            Items.Clear();
            Items.Add("Item 1");
            Items.Add("Item 2");
            Items.Add("Item 3");
            Items.Add("Item 4");
            Items.Add("Item 5");
        }

        public override async Task RefreshAsync()
        {
            await base.RefreshAsync();

            _logger.LogInformation("List VM Refresh");

            // In a real app, you can use this method to receive updated data or to
            // know when a refresh of the data in this view model is required. If
            // you don't need to refresh any data when this view model is returned
            // to, then you can simply not implement this method.
        }

        private async Task OpenItemAsync(string? item)
        {
            _logger.LogInformation($"List VM Open Item: {item}");

            await NavigatorService.PushAsync<ItemDetailPage>(item);
        }
    }
}
