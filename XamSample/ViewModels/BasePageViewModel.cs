﻿using System.Threading.Tasks;
using XamSample.Services;

namespace XamSample.ViewModels
{
    /// <summary>
    /// All Page ViewModels should extend this class.
    /// </summary>
    public abstract class BasePageViewModel : BaseViewModel
    {
        /// <summary>
        /// Navigator service for moving between pages (and opening/closing modals)
        /// </summary>
        protected INavigatorService NavigatorService { get; private set; }

        public BasePageViewModel(INavigatorService navigatorService)
        {
            NavigatorService = navigatorService;
        }

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        /// <summary>
        /// Initialize method is called from the Navigator Service and allows data to be loaded when
        /// the page is loaded, but using async methods that could not be done in the constructor.
        /// </summary>
        /// <param name="initialData"></param>
        /// <returns></returns>
        public virtual async Task InitializeAsync(object? initialData = null) { }

        /// <summary>
        /// This method is called by the Navigator Service when the page is popped to (either from
        /// standard navigation or modal navigation) and can be overridden to perform a refresh on
        /// some data in the view model after the normal initialization has already been done.
        /// </summary>
        /// <param name="updatedData"></param>
        /// <returns></returns>
        public virtual async Task RefreshAsync() { }
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
    }
}
